package com.app.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class ViewController {

	public void m1() {
		System.out.println("Branch Method");
		System.out.println("this is conflict line");
		System.out.println("next line");
		System.out.println("this is new changes");
		System.out.println("this is master branch");
		System.out.println("branch Name : edit-conflict");
	}

	@RequestMapping("dashboard")
	public String dashboard() {
		return "employee//dashboard";
	}

	@RequestMapping("about")
	public String about() {
		return "employee//about";
	}

	@RequestMapping("contact")
	public String contact() {
		return "employee//contact";
	}
}
